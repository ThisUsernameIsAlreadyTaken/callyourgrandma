package com.example.myfirstapp;

import android.util.Log;

import com.example.myfirstapp.notification.CycleType;
import com.example.myfirstapp.notification.NotificationObj;
import com.example.myfirstapp.notification.NotificationType;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;



public class Saver {
    private static final Saver ourInstance = new Saver();

    public static Saver getInstance() {
        return ourInstance;
    }

    private File file;
    private ArrayList<NotificationObj> notifications;

    // contructor
    private Saver() {
        this.notifications = new ArrayList<NotificationObj>();

        try{
            String path = App.context.getFilesDir().getAbsolutePath();
        FileInputStream fi = new FileInputStream(new File(path + "/notifications.txt"));
        ObjectInputStream oi = new ObjectInputStream(fi);

        NotificationObj newObj;
        while ((newObj = (NotificationObj) oi.readObject()) != null){
                this.notifications.add(newObj);
            }
            System.out.println("Notifications geladen");
            Log.v("info", "Notification gelesem");

    } catch (FileNotFoundException e) {
        System.out.println("File not found");
            System.out.println("Using Testnotifications");
    } catch (IOException e) {
        System.out.println("Error initializing Input stream");
        e.printStackTrace();
    } catch (ClassNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }


        // Testnotifications
        Date date = new GregorianCalendar(2019, Calendar.JUNE, 24).getTime();

        NotificationObj n1 = new NotificationObj(1,"Dunaj Nagel", NotificationType.WHATSAPP, CycleType.DAY, 1, date, " no notes");
        NotificationObj n2 = new NotificationObj(2,"Justus Jonas", NotificationType.MEET, CycleType.DAY, 1, date, " no notes");
        NotificationObj n3 = new NotificationObj(3,"Peter Wackel", NotificationType.SMS, CycleType.DAY, 1, date, " no notes");
        NotificationObj n4 = new NotificationObj(4, "Petra Meier", NotificationType.CALL, CycleType.DAY, 1, date, " " +
                "Petra hatte neulich erst Geburtstag und wollte, " +
                "dass Hannelore ihren Mann nicht mitbringt, " +
                "Frag nach wie es Lief!" +
                "       " +
                "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor" +
                " invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam" +
                " et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est " +
                "Lorem ipsum dolor sit amet.");
        n1.contact = new Contact(-1, "Dunja Nagel", new String[]{"+4915779271579"});
        n2.contact = new Contact(-1, "Justus Jonas", new String[]{"142452312"});
        n3.contact = new Contact(-1, "Peter Wackel", new String[]{"012345667"});
        n4.contact = new Contact(-1, "Petra Meier", new String[]{"142452312"});
        /*this.notifications.add(n1);
        this.notifications.add(n2);
        this.notifications.add(n3);
        this.notifications.add(n4);*/
    }

    /**
     * speichert ein NotificationObject das übergeben wurde
     *
     * @param notificationObj
     * @return
     */
    public boolean save(NotificationObj notificationObj) {

        boolean alreadyexists = false;

        // wenn schon existiert, löschen  danach neues  obj in array List
        for (NotificationObj obj : notifications) {
            if (obj.id == notificationObj.id) {
                alreadyexists = true;
            }
        }

        if(alreadyexists){
            notifications.remove(notificationObj);
        }

        this.notifications.add(notificationObj);

        return true;
    }

    /**
    löscht eine Erinnerung aus der Arraylist
     */
    public boolean delete(int id){

        boolean deleted = false;

        for (NotificationObj obj : notifications) {
            if (obj.id == id) {
                notifications.remove(obj);
                deleted = true;
            }
        }
        return deleted;
    }

    /**
     * gibt das NotoficationObjekt mit der ID zurück
     *
     * @param id
     * @return notificationobject oder null wenn das object nicht existiert
     */
    public NotificationObj load(int id) {

        NotificationObj returnobj = null;
        for (NotificationObj obj : notifications) {
            if (obj.id == id) {
                returnobj = obj;
            }
        }
        return returnobj;
    }

    /**
     * gibt alle Notifications zurück
     *
     * @return ArrayList of all Notifications
     */
    public ArrayList<NotificationObj> getAllNotifications() {
        return notifications;
    }


    /**
     * speichert die Notifications in ein File
     * @return boolean
     */
    public boolean saveNotificationstoFile() {

        boolean save = false;

        try {
            String path = App.context.getFilesDir().getAbsolutePath();
            FileOutputStream f = new FileOutputStream(new File(path + "/notifications.txt"));
            ObjectOutputStream o = new ObjectOutputStream(f);

            // Write objects to file
            for (NotificationObj obj: notifications) {
                o.writeObject(obj);
            }

            //Null Object to mark end of file
            o.writeObject(null);

            o.close();
            f.close();
            save = true;
            System.out.println("Notifications gespeichert");

        } catch (FileNotFoundException e) {
            System.out.println("File to save in not found");
        } catch (IOException e) {
            System.out.println("Error initializing Output stream");
            e.printStackTrace();
        }

        return save;
    }
}
