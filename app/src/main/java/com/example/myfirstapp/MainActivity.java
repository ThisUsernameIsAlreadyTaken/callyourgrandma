package com.example.myfirstapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.example.myfirstapp.Service.JobSchedulerForService;
import com.example.myfirstapp.calendar.CalendarFragment;
import com.example.myfirstapp.notification.NotificationObj;
import com.example.myfirstapp.notifications.NotificationsFragment;
import com.example.myfirstapp.notifications.configuration.ConfigurationFragment;
import com.example.myfirstapp.statistics.StatisticsFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NotificationsFragment.OnNotificationsFragmentInteractionListener, CalendarFragment.OnCalendarFragmentInteractionListener, StatisticsFragment.OnStatisticsFragmentInteractionListener, ConfigurationFragment.OnConfigurationFragmentInteractionListener{

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_notifications:
                    getSupportFragmentManager().beginTransaction().replace(R.id.content, new NotificationsFragment()).commit();
                    return true;
                case R.id.navigation_statistics:
                    getSupportFragmentManager().beginTransaction().replace(R.id.content, new StatisticsFragment()).commit();
                    return true;
                case R.id.navigation_calendar:
                    getSupportFragmentManager().beginTransaction().replace(R.id.content, new CalendarFragment()).commit();
                    return true;
            }
            return false;
        }
    };
    private String TAG = "bla";

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        getSupportFragmentManager().popBackStack();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        permissionRequest();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Configuration configuration = getResources().getConfiguration();

        if(configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            NotificationsFragment notificationsFragment = new NotificationsFragment();
            fragmentTransaction.replace(R.id.content, notificationsFragment);
        } else {
            NotificationsFragment notificationsFragment = new NotificationsFragment();
            fragmentTransaction.replace(R.id.content, notificationsFragment);
        }
        fragmentTransaction.commit();

        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    startScheduler(this, null);
    }
    public void startScheduler(Context context, Intent intent) {
        Log.i(TAG, "onReceive before scheduleJob");
        JobSchedulerForService.scheduleJob(context);
        Log.i(TAG, "onReceive after scheduleJob");
    }

    public void onClickAdd(View view){
        getSupportFragmentManager().beginTransaction().replace(R.id.content, ConfigurationFragment.newInstance(new NotificationObj())).addToBackStack(null).commit();
    }

    public void permissionRequest() {

        if((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
                        != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED)) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS,
                        Manifest.permission.SEND_SMS,
                        Manifest.permission.CALL_PHONE},1);

        }
    }

    public ArrayList<NotificationObj> getNotifications(){
        return Saver.getInstance().getAllNotifications();
    }

    @Override
    public void onClickItem(NotificationObj item) {
        //startActivity(new Intent(this, NotificationConfigurationActivity.class));
        getSupportFragmentManager().beginTransaction().replace(R.id.content, ConfigurationFragment.newInstance(item)).addToBackStack(null).commit();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Saver.getInstance().saveNotificationstoFile();
    }

    @Override
    public void onStop(){
        super.onStop();
        Saver.getInstance().saveNotificationstoFile();
    }

    @Override
    public void onStatisticsFragmentInteraction() {

    }

    @Override
    public void onNotificationsFragmentInteraction() {

    }

    @Override
    public void onCalendarFragmentInteraction() {

    }

    @Override
    public void onConfigurationFragmentInteraction() {
    }

    @Override
    public void onBackPressed() {
        getSupportFragmentManager().popBackStack();
    }
}
