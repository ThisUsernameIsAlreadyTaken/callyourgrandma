package com.example.myfirstapp;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;

public class Communication extends ContextWrapper {

    /**
     * c = activity
     * @param c
     */
    public Communication(Context c) {
        super(c);
    }

    public boolean call(String number) {
        Intent i = new Intent(Intent.ACTION_CALL);
        i.setData(Uri.parse("tel:" + number));
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        boolean b = ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED;
        if(!b)
            startActivity(i);
        return !b;
    }

    public boolean message(String number) {

        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setData(Uri.parse("smsto:" + Uri.encode(number)));
        boolean b = ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED;
        if(!b)
            startActivity(i);
        return !b;

    }
}
