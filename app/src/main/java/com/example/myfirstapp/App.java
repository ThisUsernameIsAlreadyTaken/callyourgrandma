package com.example.myfirstapp;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.example.myfirstapp.Service.PushNotificationService;

public class App extends Application {
    public static final String CHANNEL_1_ID = "pushNotification1";
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        //create notification channels
        createNotificationChannels();
    }

    private void createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel1 = new NotificationChannel(
                    CHANNEL_1_ID,
                    "CallYourGrandma",
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel1.setDescription("This is a push notification");
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel1);
        }
    }

    public Context getContext(){
        return context;
    }
}
