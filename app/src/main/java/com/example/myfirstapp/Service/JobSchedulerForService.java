package com.example.myfirstapp.Service;

import android.annotation.SuppressLint;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.util.Log;

import static android.content.Context.JOB_SCHEDULER_SERVICE;

public class JobSchedulerForService {
    // schedule the start of the service every 10 - 30 seconds
    @SuppressLint("NewApi")
    public static void scheduleJob(Context context) {
        Log.i("JobScheduler", "JOBSCHEDULER WURDE GESTARTET");
        ComponentName serviceComponent = new ComponentName(context, PushNotificationService.class);
        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
        builder.setMinimumLatency(1 * 10000); // wait at least
        builder.setOverrideDeadline(3 * 10000); // maximum delay
        JobScheduler jobScheduler = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            jobScheduler = context.getSystemService(JobScheduler.class);
        } else {
            jobScheduler = (JobScheduler) context.getSystemService(JOB_SCHEDULER_SERVICE);
        }

        jobScheduler.schedule(builder.build());
    }
}
