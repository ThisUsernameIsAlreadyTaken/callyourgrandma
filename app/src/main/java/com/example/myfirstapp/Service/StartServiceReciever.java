package com.example.myfirstapp.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class StartServiceReciever extends BroadcastReceiver {

    private final String TAG = "MyStartServiceReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onReceive before scheduleJob");
        JobSchedulerForService.scheduleJob(context);
        Log.i(TAG, "onReceive after scheduleJob");
    }
}
