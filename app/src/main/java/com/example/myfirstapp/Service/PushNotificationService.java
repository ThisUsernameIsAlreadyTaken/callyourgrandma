package com.example.myfirstapp.Service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.example.myfirstapp.R;
import com.example.myfirstapp.Saver;
import com.example.myfirstapp.notification.NotificationObj;
import com.example.myfirstapp.notification.NotificationType;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.example.myfirstapp.App.CHANNEL_1_ID;

@SuppressLint("NewApi")
public class PushNotificationService extends JobService {
    private static final String TAG = "SyncService";

    @Override
    public boolean onStartJob(JobParameters params) {
        List<NotificationObj> temp = new CopyOnWriteArrayList<>();

        for(NotificationObj o : getNotifications()){
            temp.add(o);
        }
        //Intent service = new Intent(getApplicationContext(), LocalWordService.class);
        //getApplicationContext().startService(service);
        Log.i(TAG, "SERVICE WURDE GESTARTET");
        JobSchedulerForService.scheduleJob(getApplicationContext()); // reschedule the job

        for (NotificationObj obj : temp) {
            Date notificationDate = obj.startdate;

            Calendar calendarStartDate = Calendar.getInstance();
            calendarStartDate.setTime(notificationDate);

            Calendar calenderToday = Calendar.getInstance();

            switch (obj.cycle) {
                case DAY:
                    calendarStartDate.add(Calendar.DATE, obj.cyclenumber);
                    break;
                case WEEK:
                    calendarStartDate.add(Calendar.WEEK_OF_MONTH, obj.cyclenumber);
                    break;
                case MONTH:
                    calendarStartDate.add(Calendar.MONTH, obj.cyclenumber);
                    break;
                case YEAR:
                    calendarStartDate.add(Calendar.YEAR, obj.cyclenumber);
                    break;
            }

            if (calenderToday.get(Calendar.DAY_OF_YEAR) == calendarStartDate.get(Calendar.DAY_OF_YEAR)
                    && calenderToday.get(Calendar.YEAR) == calendarStartDate.get(Calendar.YEAR)) {

                showNotification(obj);
                obj.startdate = calenderToday.getTime();
                Saver.getInstance().save(obj);
            }
        }

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

    /**
     * /Get an ArrayList from the Saver with all notifications
     *
     * @return ArrayList with all NotificationObjects
     */
    public ArrayList<NotificationObj> getNotifications() {
        return Saver.getInstance().getAllNotifications();
    }

    /**
     * Builds a Notification with the data from the NotificationObj
     *
     * @param notificationObj
     */
    public void showNotification(NotificationObj notificationObj) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_1_ID)
                .setContentTitle(notificationObj.contactName)
                .setContentText(notificationObj.notes)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(notificationObj.notes))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        if (notificationObj.type == NotificationType.CALL) {
            builder.setSmallIcon(R.drawable.telephone);
            Bitmap phone = BitmapFactory.decodeResource(getResources(), R.drawable.telephone);
            builder.setLargeIcon(phone);
            Uri uri = Uri.parse("tel:"+ notificationObj.contact.getNumbers()[0]);
            Intent sendIntent = new Intent(Intent.ACTION_DIAL, uri);
            PendingIntent intent = PendingIntent.getActivity(this, 0, sendIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(intent);
        } else if (notificationObj.type == NotificationType.WHATSAPP) {
            builder.setSmallIcon(R.drawable.whatsapp);
            Bitmap whatsApp = BitmapFactory.decodeResource(getResources(), R.drawable.whatsapp);
            builder.setLargeIcon(whatsApp);
            Uri uri = Uri.parse("https://api.whatsapp.com/send?phone="+notificationObj.contact.getNumbers()[0]);
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setData(uri);
            PendingIntent intent = PendingIntent.getActivity(this, 0, sendIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(intent);
        } else if (notificationObj.type == NotificationType.SMS) {
            builder.setSmallIcon(R.drawable.chat);
            Bitmap chat = BitmapFactory.decodeResource(getResources(), R.drawable.chat);
            builder.setLargeIcon(chat);
            Uri uri = Uri.parse("smsto:" + notificationObj.contact.getNumbers()[0]);
            Intent sendIntent = new Intent(Intent.ACTION_SENDTO, uri);
            sendIntent.putExtra("sms_body", "Hi, " + notificationObj.contact.getName());
            PendingIntent intent = PendingIntent.getActivity(this, 0, sendIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(intent);
        } else if (notificationObj.type == NotificationType.MEET) {
            builder.setSmallIcon(R.drawable.interview);
            Bitmap meeting = BitmapFactory.decodeResource(getResources(), R.drawable.interview);
            builder.setLargeIcon(meeting);
        } else if (notificationObj.type == NotificationType.INVITEFORBEER) {
            builder.setSmallIcon(R.drawable.beer);
            Bitmap beer = BitmapFactory.decodeResource(getResources(), R.drawable.beer);
            builder.setLargeIcon(beer);
        }

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        notificationManager.notify(notificationObj.id, builder.build());
    }

}