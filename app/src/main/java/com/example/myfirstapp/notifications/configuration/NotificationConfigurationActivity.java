package com.example.myfirstapp.notifications.configuration;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

import com.example.myfirstapp.MainActivity;
import com.example.myfirstapp.R;
import com.example.myfirstapp.notifications.configuration.ConfigurationFragment;

public class NotificationConfigurationActivity extends AppCompatActivity implements ConfigurationFragment.OnConfigurationFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        ConfigurationFragment configurationFragment = new ConfigurationFragment();
        fragmentTransaction.replace(R.id.content, configurationFragment);
        fragmentTransaction.commit();
        setContentView(R.layout.activity_notification_configuration);
    }

    @Override
    public void onConfigurationFragmentInteraction() {

    }
}
