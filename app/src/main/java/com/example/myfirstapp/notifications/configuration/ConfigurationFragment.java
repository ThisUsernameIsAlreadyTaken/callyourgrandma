package com.example.myfirstapp.notifications.configuration;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfirstapp.Contact;
import com.example.myfirstapp.MainActivity;
import com.example.myfirstapp.R;
import com.example.myfirstapp.Saver;
import com.example.myfirstapp.notification.CycleType;
import com.example.myfirstapp.notification.NotificationObj;
import com.example.myfirstapp.notification.NotificationType;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnConfigurationFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ConfigurationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConfigurationFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM_CONTACT = "param_contact";
    private static final String ARG_PARAM_TYPE = "param_type";
    private static final String ARG_PARAM_CYCLE_TYPE = "param_cycle_type";
    private static final String ARG_PARAM_CYCLE_NUMBER = "param_cycle_number";
    private static final String ARG_PARAM_DATE = "param_date";
    private static final String ARG_PARAM_NOTES = "param_notes";

    public String mContact;
    public NotificationType mType;
    public CycleType mCycleType;
    public int mCycleNumber;
    public Date mStartDate;
    public String mNotes;
    public NotificationObj notification;

    private OnConfigurationFragmentInteractionListener mListener;

    public ConfigurationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param not NotificationObj.
     * @return A new instance of fragment ConfigurationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConfigurationFragment newInstance(NotificationObj notification) {
        ConfigurationFragment fragment = new ConfigurationFragment();
        Bundle args = new Bundle();
        fragment.notification = notification;
        args.putString(ARG_PARAM_CONTACT, notification.contactName);
        args.putString(ARG_PARAM_TYPE, notification.type.toString());
        args.putString(ARG_PARAM_CYCLE_TYPE, notification.cycle.toString());
        args.putInt(ARG_PARAM_CYCLE_NUMBER, notification.cyclenumber);
        args.putString(ARG_PARAM_DATE, notification.startdate.toString());
        args.putString(ARG_PARAM_NOTES, notification.notes);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //mContact = getArguments().getString(ARG_PARAM_CONTACT);
            //mType = NotificationType.values()[getArguments().getString(ARG_PARAM_TYPE)];
            //mCycleType = getArguments().getString();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_configuration, container, false);
        ((TextView) v.findViewById(R.id.notesView)).setText(this.notification.notes);
        final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        ((TextView) v.findViewById(R.id.dateView)).setText(format.format(this.notification.startdate));
        ((TextView) v.findViewById(R.id.notesView)).setText(this.notification.notes);

        Spinner contactsSpinner = v.findViewById(R.id.contactsSpinner);
        ContactsSpinnerAdapter contactsAdapter = new ContactsSpinnerAdapter(this.getContext(), Contact.getContactList(getActivity()));
        contactsSpinner.setAdapter(contactsAdapter);
        if (this.notification.contact != null) {
            int position = contactsAdapter.getPosition(this.notification.contact);
            System.out.println(position);
            contactsSpinner.setSelection(position);
        }
        contactsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                notification.contact = Contact.getContactList(getActivity()).get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //notification.contact = Contact.getContactList(getActivity()).get(0);
            }
        });

        Spinner contactTypeSpinner = ((Spinner) v.findViewById(R.id.contactTypeView));
        ArrayAdapter contactTypeAdapter = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_spinner_item, NotificationType.toStringAll());
        contactTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        contactTypeSpinner.setAdapter(contactTypeAdapter);
        if (this.notification.type != null) {
            int position = contactTypeAdapter.getPosition(this.notification.type.toString());
            contactTypeSpinner.setSelection(position);
        }
        contactTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                notification.type = NotificationType.values()[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                notification.type = NotificationType.values()[0];
            }
        });

        Spinner cycleTypeSpinner = ((Spinner) v.findViewById(R.id.cycleTypeView));
        ArrayAdapter cycleTypeAdapter = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_spinner_item, CycleType.toStringAll());
        cycleTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cycleTypeSpinner.setAdapter(cycleTypeAdapter);
        if (this.notification.cycle != null) {
            int position = cycleTypeAdapter.getPosition(this.notification.cycle.toString());
            cycleTypeSpinner.setSelection(position);
        }
        cycleTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                notification.cycle = CycleType.values()[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                notification.cycle = CycleType.values()[0];
            }
        });

        ArrayAdapter cycleNumberAdapter = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_spinner_item, this.notification.cycle.getValues());
        Spinner cycleNumberSpinner = ((Spinner) v.findViewById(R.id.cycleNumberView));
        cycleNumberAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cycleNumberSpinner.setAdapter(cycleNumberAdapter);
        if (this.notification.cyclenumber != -1) {
            int position = cycleNumberAdapter.getPosition(this.notification.cyclenumber);
            cycleNumberSpinner.setSelection(position);
        }
        cycleNumberSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                notification.cyclenumber = notification.cycle.getValues()[position];

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                notification.cyclenumber = notification.cycle.getValues()[0];
            }
        });

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                ((TextView) v.findViewById(R.id.dateView)).setText(format.format(myCalendar.getTime()));
                notification.startdate = myCalendar.getTime();
            }

        };
        v.findViewById(R.id.calendarButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getContext(), date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        ImageButton buttonDelete = (ImageButton) v.findViewById(R.id.buttonDelete);
        buttonDelete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(final View v) {
                onClickDelete(v);
            }
        });
        Button buttonSave = (Button) v.findViewById(R.id.buttonSave);
        final TextView notes = ((TextView)v.findViewById(R.id.notesView));
        buttonSave.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(final View v) {
                onClickSave(v, notes.getText().toString());
            }
        });
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (context instanceof OnConfigurationFragmentInteractionListener) {
            mListener = (OnConfigurationFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnConfigurationFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnConfigurationFragmentInteractionListener {
        // TODO: Update argument type and name
        void onConfigurationFragmentInteraction();

    }


    private boolean onClickSave(View v, String notes) {
        this.notification.notes = notes;
        Saver.getInstance().save(this.notification);
        getActivity().onBackPressed();
        return false;
    }

    private boolean onClickDelete(View v) {
        final int id = this.notification.id;
        new AlertDialog.Builder(this.getContext())
                .setTitle("Löschen")
                .setMessage("Möchtest du die Benachrichtugung wirklich löschen?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        Toast.makeText(getContext(), "Gelöscht", Toast.LENGTH_LONG).show();
                        Saver.getInstance().delete(id);
                        getActivity().onBackPressed();
                    }})
                .setNegativeButton(android.R.string.no, null).show();
        return false;
    }

}
