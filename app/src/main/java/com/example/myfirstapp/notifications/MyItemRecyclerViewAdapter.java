package com.example.myfirstapp.notifications;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfirstapp.notification.NotificationObj;
import com.example.myfirstapp.notifications.NotificationListFragment.OnListFragmentInteractionListener;
import com.example.myfirstapp.R;

import java.text.SimpleDateFormat;
import java.util.List;

public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private final List<NotificationObj> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyItemRecyclerViewAdapter(List<NotificationObj> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        switch (mValues.get(position).type){
            case WHATSAPP: holder.mImageView.setImageResource(R.drawable.whatsapp);
                break;
            case CALL: holder.mImageView.setImageResource(R.drawable.telephone);
                break;
            case SMS: holder.mImageView.setImageResource(R.drawable.chat);
                break;
            case MEET: holder.mImageView.setImageResource(R.drawable.interview);
                break;
            case INVITEFORBEER: holder.mImageView.setImageResource(R.drawable.beer);
                break;
        }

        holder.mIdView.setText(mValues.get(position).contact.getName());
        holder.mContentView.setText(mValues.get(position).contact.getNumbers()[0]);
        final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        holder.mDate.setText(format.format(mValues.get(position).startdate));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onClickItem(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final TextView mDate;
        public NotificationObj mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.itemImage);
            mIdView = (TextView) view.findViewById(R.id.itemName);
            mContentView = (TextView) view.findViewById(R.id.itemDescription);
            mDate = (TextView) view.findViewById(R.id.itemDate);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
