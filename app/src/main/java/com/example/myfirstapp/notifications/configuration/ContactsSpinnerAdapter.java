package com.example.myfirstapp.notifications.configuration;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.myfirstapp.Contact;

import java.util.ArrayList;

public class ContactsSpinnerAdapter extends ArrayAdapter<Contact> {

    public ContactsSpinnerAdapter(Context context, ArrayList<Contact> contacts) {
        super(context, -1, contacts);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    public View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    android.R.layout.simple_spinner_dropdown_item, parent, false
            );
        }

        TextView textView = convertView.findViewById(android.R.id.text1);
        textView.setText(getItem(position).getName());
    return convertView;
    }

}
