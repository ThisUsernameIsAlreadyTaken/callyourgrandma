package com.example.myfirstapp.notification;

import android.icu.util.Calendar;

import com.example.myfirstapp.Contact;

import java.io.Serializable;
import java.util.Date;

public class NotificationObj implements Serializable {

    public int id;
    public String contactName;
    public NotificationType type;
    public CycleType cycle;
    public int cyclenumber;
    public Date startdate;
    public String notes;
    public boolean active;
    public Contact contact;

    public NotificationObj(int id, String contactName, NotificationType type, CycleType c, int cyclenumber, Date startdate, String notes){
        this.id = id;
        this.contactName = contactName;
        this.type = type;
        this.cycle = c;
        this.cyclenumber = cyclenumber;
        this.startdate = startdate;
        this.notes = notes;
        this.active = true;
    }

    public NotificationObj(){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, 7);;
        this.type = NotificationType.CALL;
        this.cycle = CycleType.WEEK;
        this.cyclenumber = this.cycle.getValues()[0];
        this.startdate = cal.getTime();
        this.active = true;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return "ID:" + this.id + "\nContact: " + this.contactName + "\nNotificationtype_ " + this.type.toString()
                + "\nCycle: " + this.cycle.toString() + "\nCyclenr: " + this.cyclenumber + "\nStartdate: " + this.startdate.toString()
                + "\nNotes: " + this.notes + "\nActive: " + this.active;
    }
}
