package com.example.myfirstapp.notification;

public enum CycleType implements EnumValuesInterface {
    DAY {
        @Override
        public Integer[] getValues() {
            return new Integer[]{1, 2, 3, 4, 5, 6};
        }
    },
    WEEK {
        @Override
        public Integer[] getValues() {
            return new Integer[]{1, 2, 3, 4, 5, 6};
        }
    },
    MONTH {
        @Override
        public Integer[] getValues() {
            return new Integer[]{1, 2, 3, 4, 5, 6};
        }
    },
    YEAR {
        @Override
        public Integer[] getValues() {
            return new Integer[]{1, 2, 3, 4, 5, 6};
        }
    };

    public static String[] toStringAll(){
        String[] res = new String[CycleType.values().length];
        for (int i = 0; i < res.length; i++){
            res[i] = CycleType.values()[i].toString();
        }
        return res;
    }
}

interface EnumValuesInterface {
    Integer[] getValues();
}
