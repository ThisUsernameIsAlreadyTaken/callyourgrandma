package com.example.myfirstapp.notification;

public enum NotificationType {
    CALL,
    WHATSAPP,
    MEET,
    SMS,
    INVITEFORBEER;

    public static String[] toStringAll(){
        String[] res = new String[NotificationType.values().length];
        for (int i = 0; i < res.length; i++){
            res[i] = NotificationType.values()[i].toString();
        }
        return res;
    }
}
