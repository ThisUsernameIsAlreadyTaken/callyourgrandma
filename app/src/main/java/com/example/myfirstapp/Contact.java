package com.example.myfirstapp;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Contact {

    public static ArrayList<Contact> getContactList(Context c) {
        if(c == null)
        Log.d("contact", "shit");
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        Cursor contactCursor = null;
        ContentResolver cr = c.getContentResolver();
        try {
            contactCursor = cr.query(ContactsContract.Contacts.CONTENT_URI,null,null,null,null);
        } catch (Exception e) {
            Log.d("contacterror", "Enable Permission");
            ArrayList<Contact> permissiondenied = new ArrayList<Contact>();
            permissiondenied.add(new Contact(-1, "No Permission","",""));
            return permissiondenied;
        }
        while (contactCursor.moveToNext()) {
            int id = Integer.parseInt(contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts._ID)));
            String name = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            ArrayList<String> numbers = new ArrayList<String>();
            Cursor numberCursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{""+id},null);
            int i = 0;
            while(numberCursor.moveToNext()) {
                numbers.add(numberCursor.getString(numberCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                i++;
            }
            numberCursor.close();
            String[] temp = new String[numbers.size()];
            for (int j = 0; j < numbers.size(); j++) {
                temp[j] = numbers.get(j);
            }
            contacts.add(new Contact(id, name, temp));
        }
        for (Contact con:contacts) {
            Log.d("contactdebug", contacts.toString());
        }
        return contacts;
    }

    private int id;
    private String[] numbers;
    private String name;

    public Contact(int id, String name, String... number) {
        this.id = id;
        this.name = name;
        this.numbers = number;
    }

    public String getName() { return name; }
    public String[] getNumbers() { return numbers; }
    public int getId() { return this.id; }

    @Override
    public String toString() {
        return id + " " + name + "\n" + Arrays.toString(numbers);
    }

    @Override
    public boolean equals(Object obj){
        return ((Contact)obj).id == this.id;
    }
}